package br.mviccari.bagualapps.HelloWorld;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

/**
 * Created by Avell G1711 NEW on 24/05/2014.
 */
public class TelaDois extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.teladois);


    }

    public void abrirCalculadora(View view) {
        Intent intent = new Intent(TelaDois.this, Calculadora.class);
        startActivity(intent);
    }
}