package br.mviccari.bagualapps.HelloWorld;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.text.DecimalFormat;

/**
 * Created by Avell G1711 NEW on 24/05/2014.
 */
@SuppressWarnings("ConstantConditions")
public class Calculadora extends Activity {

    private Double valorCalculado;
    private EditText calculadoraCampoTexto;
    private Boolean devoApagarValorNoProximoNumeroDigitado = false;
    private TipoOperacao tipoOperacao = TipoOperacao.SOMA;

    private enum TipoOperacao{
        SOMA, SUBTRAI, MULTIPLICA, DIVIDE;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.calculadora);
        calculadoraCampoTexto = (EditText) findViewById(R.id.calculadoraCampoTexto);
        colocaAcaoDigitaNumeros(
                R.id.calculadoraBotaoPonto,
                R.id.calculadoraBotaoZero,
                R.id.calculadoraBotaoUm,
                R.id.calculadoraBotaoDois,
                R.id.calculadoraBotaoTres,
                R.id.calculadoraBotaoQuatro,
                R.id.calculadoraBotaoCinco,
                R.id.calculadoraBotaoSeis,
                R.id.calculadoraBotaoSete,
                R.id.calculadoraBotaoOito,
                R.id.calculadoraBotaoNove
        );

        if(savedInstanceState!=null) {
            String valorSalvo = savedInstanceState.getString("resultado");
            calculadoraCampoTexto.setText(valorSalvo);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString("resultado", calculadoraCampoTexto.getText().toString());
        super.onSaveInstanceState(outState);
    }

    public void colocaAcaoDigitaNumeros(int... ids) {
        for (int id : ids) {
            final Button botao = (Button) findViewById(id);
            botao.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (devoApagarValorNoProximoNumeroDigitado) {
                        calculadoraCampoTexto.setText(null);
                        devoApagarValorNoProximoNumeroDigitado = false;
                    }
                    calculadoraCampoTexto.setText(calculadoraCampoTexto.getText().append(botao.getText()));
                    calculadoraCampoTexto.setSelection(calculadoraCampoTexto.getText().length());
                }
            });
        }
    }

    public void soma(View view) {
        tipoOperacao = TipoOperacao.SOMA;
        finalizarCalculo();
    }

    public void subtrai(View view) {
        tipoOperacao = TipoOperacao.SUBTRAI;
        finalizarCalculo();
    }

    public void divide(View view) {
        tipoOperacao = TipoOperacao.DIVIDE;
        finalizarCalculo();
    }

    public void multiplica(View view) {
        tipoOperacao = TipoOperacao.MULTIPLICA;
        finalizarCalculo();
    }

    public void igual(View view) {
        finalizarCalculo();
    }

    public void finalizarCalculo() {
        Double valorDigitado = Double.parseDouble(calculadoraCampoTexto.getText().toString());
        if(valorCalculado==null){
            valorCalculado = valorDigitado;
        }else {
            if (tipoOperacao.equals(TipoOperacao.SOMA)) {
                valorCalculado = valorCalculado + valorDigitado;
            } else if (tipoOperacao.equals(TipoOperacao.SUBTRAI)) {
                valorCalculado = valorCalculado - valorDigitado;
            } else if (tipoOperacao.equals(TipoOperacao.MULTIPLICA)) {
                valorCalculado = valorCalculado * valorDigitado;
            } else {
                valorCalculado = valorCalculado / valorDigitado;
            }
        }
        devoApagarValorNoProximoNumeroDigitado = true;
        calculadoraCampoTexto.setText(valorCalculado.toString());
        calculadoraCampoTexto.setSelection(calculadoraCampoTexto.getText().length());
    }

}